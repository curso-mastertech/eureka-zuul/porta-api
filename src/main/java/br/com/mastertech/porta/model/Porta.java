package br.com.mastertech.porta.model;

import javax.persistence.*;

@Entity
@Table(name = "portas")
public class Porta {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String andar;
    private String sala;

    public Porta() {
    }

    public Porta(String andar, String sala) {
        this.andar = andar;
        this.sala = sala;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
