package br.com.mastertech.porta.controller;

import br.com.mastertech.porta.model.Porta;
import br.com.mastertech.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    public Porta create(@Valid @RequestBody Porta porta) {
        return portaService.create(porta);
    }

    @GetMapping("/{id}")
    public Porta getById(@PathVariable Long id) {
        return portaService.findById(id);
    }


}
