package br.com.mastertech.porta.repository;

import br.com.mastertech.porta.model.Porta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PortaRepository extends JpaRepository<Porta, Long> {

}
