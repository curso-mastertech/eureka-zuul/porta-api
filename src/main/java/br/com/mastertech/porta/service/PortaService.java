package br.com.mastertech.porta.service;

import br.com.mastertech.porta.exception.PortaNotFoundException;
import br.com.mastertech.porta.model.Porta;
import br.com.mastertech.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta create(Porta p) {
        return portaRepository.save(p);
    }

    public Porta findById(Long id) {
        return portaRepository
                .findById(id)
                .orElseThrow(() -> new PortaNotFoundException(id));
    }
}
